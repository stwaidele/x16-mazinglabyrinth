// MAZE x16
//
// Based on:
//Purpy Pupple's amazing maze generator. 
//Released under the CC-BY-SA 3.0 License and the GFDL
// https://en.wikipedia.org/wiki/User:Dllu/Maze
//
// cl65 -t cx16 -C cx16.cfg -o maze.prg maze-x16.c
// ./x16emu -keymap de -prg ../Code/X16-mazinglabyrinth/maze.prg

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <conio.h>
#include <cbm.h>

#define UP 0     //-y
#define DOWN 1   //+y
#define LEFT 2   //-x
#define RIGHT 3  //+x

#define XOFFSET_2D 40
#define YOFFSET_2D 20
#define XOFFSET_3D 0
#define YOFFSET_3D 20

// const int xsize=19;     // labyrinth.bas maximum
// const int ysize=10;     // labyrinth.bas maximum
#define xsize 19
#define ysize 10

struct cell{
	bool in;  //Is this cell in the maze?
	bool up;  //Does the wall above this cell exist?
	bool left;//Does the wall to the left of this cell exist?
	int prevx, prevy; //The coordinates of the previous cell, used for backtracking.
};

struct cell MAZE[xsize][ysize];

enum block {
    wall = 0xa0,
    path = 0x20,
    up = 0x1e,
    down = 0x16,
    left = 0x28,
    right = 0x29,
	notinmaze = 0x7f,
	blank = 0x20,
	sidewall_left = 0x20,
	sidewall_left_bottom = 0xe9,
	sidewall_left_top = 0x4d,
	sidewall_left_side = 0x67,
	sidewall_right = 0x20,
	sidewall_right_bottom = 0xdf,
	sidewall_right_top = 0x4e,
	sidewall_right_side = 0x65,
	bottom = 0xa0,
	bottom_forward = 0xf7,
	bottom_left = 0xce,
	bottom_right = 0xcd,
	frontwall = 0x66,
	slimwall = 0xe6,
	flattop = 0x64,
	debug = 0x56,
};

int cx, cy, counter;
int posx = 1;
int posy = 1;
int orientation = 3;

bool show_frontwall_0 = false;
bool show_frontwall_1 = false;
bool show_frontwall_2 = false;
bool show_frontwall_3 = false;
bool show_frontwall_4 = false;
bool show_frontwall_5 = false;
bool show_leftfrontwall_0 = false;
bool show_leftfrontwall_1 = false;
bool show_leftfrontwall_2 = false;
bool show_leftfrontwall_3 = false;
bool show_leftfrontwall_4 = false;
bool show_leftslim_0 = false;
bool show_leftslim_1 = false;
bool show_leftslim_2 = false;
bool show_leftslim_3 = false;
bool show_leftslim_4 = false;
bool show_lefttile_0 = false;
bool show_lefttile_1 = false;
bool show_lefttile_2 = false;
bool show_lefttile_3 = false;
bool show_lefttile_4 = false;
bool show_leftwall_0 = false;
bool show_leftwall_1 = false;
bool show_leftwall_2 = false;
bool show_leftwall_3 = false;
bool show_leftwall_4 = false;
bool show_rightfrontwall_0 = false;
bool show_rightfrontwall_1 = false;
bool show_rightfrontwall_2 = false;
bool show_rightfrontwall_3 = false;
bool show_rightfrontwall_4 = false;
bool show_rightslim_0 = false;
bool show_rightslim_1 = false;
bool show_rightslim_2 = false;
bool show_rightslim_3 = false;
bool show_rightslim_4 = false;
bool show_righttile_0 = false;
bool show_righttile_1 = false;
bool show_righttile_2 = false;
bool show_righttile_3 = false;
bool show_righttile_4 = false;
bool show_rightwall_0 = false;
bool show_rightwall_1 = false;
bool show_rightwall_2 = false;
bool show_rightwall_3 = false;
bool show_rightwall_4 = false;
bool show_tile_0 = false;
bool show_tile_1 = false;
bool show_tile_2 = false;
bool show_tile_3 = false;
bool show_tile_4 = false;

void initialize();
void generate();
void display_maze_2d(int playerx, int playery, int orientation); 
void display_maze_3d(int playerx, int playery, int orientation);
void display_player(int x, int y, int orientation);
void remove_player(int x, int y);
void navigate_absolute(int key);
void navigate_relative(int key);
void draw_2d(int x, int y, enum block element);
void draw_3d(int x, int y, enum block element);


int main(){
	int key;

    // https://www.commanderx16.com/forum/index.php?/topic/999-cc65-switch-to-uppercasepetscii/
    cbm_k_bsout(CH_FONT_UPPER);
    //seed random number generator with system time
	srand((unsigned int)time(NULL)); 
	initialize();      //initialize the maze
	generate();        //generate the maze
	display_maze_2d(posx, posy, orientation); // display the maze
	display_maze_3d(posx, posy, orientation);

    // THE LOOP
	// ...until Q is pressed 
	//
	// https://retrogamecoders.com/retro-c-code-text-games/
	// Keycodes for cursor keys: https://www.pagetable.com/c64ref/charset/
    while ((key  = cgetc()) != 'q') {
		remove_player(posx, posy);
		navigate_relative(key);
	    display_player(posx, posy, orientation);
		display_maze_3d(posx, posy, orientation);
	}
 	return 0;
}

void initialize(){
	//Initialize the maze!
    int x, y;
	for(x=0;x<xsize;x++){
		for(y=0;y<ysize;y++){
			//The maze cells on the edges of the maze are "in" to provide padding. Otherwise, all maze cells are not in.
			MAZE[x][y].in   = (x==0||x==xsize-1||y==0||y==ysize-1)?1:0;
			//All maze cells have all walls existing by default, except the perimeter cells.
			MAZE[x][y].up   = (x==0||x==xsize-1||y==0)?0:1;
			MAZE[x][y].left = (x==0||y==0||y==ysize-1)?0:1;
		}
	}
	return;
}

void generate() {	
	// Theory: https://www.baeldung.com/cs/maze-generation
	// Implementation: https://en.wikipedia.org/wiki/User:Dllu/Maze
	//
    //start growing from the corner. It could theoretically start growing from anywhere, doesn't matter.
	long numin=1;     //Number of cells (already) in the maze.
    int xcur=1, ycur=1;
	int whichway;
	bool success;
	MAZE[xcur][ycur].in = true;
	do{
		while( MAZE[xcur][ycur-1].in&&MAZE[xcur][ycur+1].in&&
			   MAZE[xcur-1][ycur].in&&MAZE[xcur+1][ycur].in ){
				   //If all the neighbourhood cells are in, backtrack.
				int xcur2=MAZE[xcur][ycur].prevx;
				ycur=MAZE[xcur][ycur].prevy;
				xcur=xcur2;
		}
		do{
			//Randomly grow the maze if possible.
			success=0;
			whichway=rand()%4;
			switch(whichway){
			case UP:
				if(!MAZE[xcur][ycur-1].in){
					success=1;
					MAZE[xcur][ycur].up=0;
					MAZE[xcur][ycur-1].prevx=xcur;
					MAZE[xcur][ycur-1].prevy=ycur;
					ycur--;
				}
				break;
			case DOWN:
				if(!MAZE[xcur][ycur+1].in){
					success=1;
					MAZE[xcur][ycur+1].up=0;
					MAZE[xcur][ycur+1].prevx=xcur;
					MAZE[xcur][ycur+1].prevy=ycur;
					ycur++;
				}
				break;
			case LEFT:
				if(!MAZE[xcur-1][ycur].in){
					success=1;
					MAZE[xcur][ycur].left=0;
					MAZE[xcur-1][ycur].prevx=xcur;
					MAZE[xcur-1][ycur].prevy=ycur;
					xcur--;
				}
				break;
			case RIGHT:
				if(!MAZE[xcur+1][ycur].in){
					success=1;
					MAZE[xcur+1][ycur].left=0;
					MAZE[xcur+1][ycur].prevx=xcur;
					MAZE[xcur+1][ycur].prevy=ycur;
					xcur++;
				}
				break;
			}
		}while(!success);
		MAZE[xcur][ycur].in=1;
		numin++; //Every iteration of this loop, one maze cell is added to the maze.
	}while(numin<(xsize-2)*(ysize-2));
	return;
}

void draw_2d(int x, int y, enum block element){
	int position, color = 0;
    
	position = ((y + YOFFSET_2D) * 0x0100 ) + (x + XOFFSET_2D) * 2;
	color = position + 1; 
	vpoke((int)element,position);
	vpoke(0x61,color);
}

void draw_3d(int x, int y, enum block element){
	int position, color = 0;
    
	position = ((y + YOFFSET_3D) * 0x0100 ) + (x + XOFFSET_3D) * 2;
	color = position + 1; 
	vpoke((int)element,position);
	vpoke(0x61,color);
}

void display_player(int x, int y, int orientation) {
	int posx=(x)*2-1;
	int posy=(y)*2-1;
	switch (orientation) {
		case UP:
        	draw_2d(posx, posy, up);
			break;
		case DOWN:
            draw_2d(posx, posy, down);
			break;
		case LEFT:
            draw_2d(posx, posy, left);
			break;
		case RIGHT:
            draw_2d(posx, posy, right);
			break;
	}
}

void remove_player(x, y) {
	int posx=(x)*2-1;
	int posy=(y)*2-1;

	draw_2d(posx, posy, path);
}

void navigate_relative(int key) {
	        switch (key) { 
            case 'W': 
            case 'w': 
			case 0x91:
				switch (orientation) {
				case UP:
					// Move up
					if (!MAZE[posx][posy].up) {
						posy--; 
					}
					break;
				case LEFT: 
					// Move left
					if (!MAZE[posx][posy].left) {
	                	posx--; 
					}
					break;
				case RIGHT:
					// Move right
					if (!MAZE[posx+1][posy].left) {
        	        	posx++; 
					}
					break;
				case DOWN:
					// Move down
					if (!MAZE[posx][posy+1].up) {
        	        	posy++; 
					}
					break;	
				} // switch (orientation)
                break; // case 'w' aka uparrow 
			// case 'a' aka leftarrow
            case 'A':
            case 'a':  
			case 0x9d:
				// Turn left
				switch (orientation) {
				case UP:
					orientation = LEFT;
					break;
				case LEFT: 
					orientation = DOWN;
					break;
				case RIGHT:
					orientation = UP;
					break;
				case DOWN:
					orientation = RIGHT;
					break;	
				} // switch (orientation)
              	break; // case 'a' aka leftarrow
			// case 'd' aka rightarrow
            case 'D':
            case 'd':
			case 0x1d: 
				// Turn right
				switch (orientation) {
				case UP:
					orientation = RIGHT;
					break;
				case LEFT: 
					orientation = UP;
					break;
				case RIGHT:
					orientation = DOWN;
					break;
				case DOWN:
					orientation = LEFT;
					break;	
				} // switch (orientation)
              	break; // case 'd' aka rightarrow
            default: 
                break; 
        }

}

void navigate_absolute(int key) {
	        switch (key) { 
            case 'W': 
            case 'w': 
			case 0x91:
				// Move up
				if (!MAZE[posx][posy].up) {
					posy--; 
				}
				orientation = UP;
                break; 
            case 'A':
            case 'a':  
			case 0x9d:
				// Move left
				if (!MAZE[posx][posy].left) {
                	posx--; 
				}
				orientation = LEFT;
                break; 
            case 'S': 
            case 's': 
			case 0x11:
				// Move down
				if (!MAZE[posx][posy+1].up) {
                	posy++; 
				}
				orientation = DOWN;
                break; 
            case 'D':
            case 'd':
			case 0x1d: 
				// Move right
				if (!MAZE[posx+1][posy].left) {
                	posx++; 
				}
				orientation = RIGHT;
                break; 
            default: 
                break; 
        }

}

void display_maze_2d(int playerx, int playery, int orientation) {
    // xspecial, yspecial ---> You are here
    int x, y;
	int width=(xsize-1)*2-1;
	int height=(ysize-1)*2-1;

	for(y = 0; y <= height - 1; y++){
		for(x = 0; x <= width - 1; x++){
			if(x%2 == 1 && y%2 == 1) {
            // We are on the path or on a wall
				if(x/2+1 == playerx && y/2+1 == playery) {
                    // ---> You are here
					display_player(x, y, orientation);
                } else {
                    // Shrink back to original size
                    // Fill according to "in" 
                    // or "out" (not possible in finished maze)
					if(MAZE[x/2+1][y/2+1].in) {
                        draw_2d(x, y, path);
                    } else {
                       draw_2d(x, y, notinmaze);
                    }
				}
            // We are "on a boundary"
			} else if (x%2 == 0 && y%2 == 0) {  
				draw_2d(x, y, wall);
			} else if (x%2 == 0 && y%2 == 1) {
				if(MAZE[x/2+1][y/2+1].left) {
                    draw_2d(x, y, wall);
                } else {
                    draw_2d(x, y, path);
                }
			} else if (x%2 == 1 && y%2 == 0) {
				if(MAZE[x/2+1][y/2+1].up) {
                    draw_2d(x, y, wall);
                } else {
                    draw_2d(x, y, path);
                }
			}
		}
    }
}

void blank_3d() {
	// Todo: Only for debugging
	for (cx = 0; cx < 36; cx++) {
		for (cy = 0; cy < 22; cy++) { 
			draw_3d(cx, cy, blank);
		}
	}
}

void draw_tile_0() {
	// Draw the tile we are standing on, level 0
	draw_3d(7, 19, sidewall_left_bottom);
	draw_3d(6, 20, sidewall_left_bottom);
	draw_3d(7, 20, bottom);
	draw_3d(5, 21, sidewall_left_bottom);
	draw_3d(6, 21, bottom);
	draw_3d(7, 21, bottom);
	for (cx = 8; cx < 28; cx++) { 
		draw_3d(cx, 19, bottom_forward);
	}
	for (cx = 8; cx < 28; cx++) {
		for (cy = 20; cy < 22; cy++) { 
			draw_3d(cx, cy, bottom);
		}
	}	
	draw_3d(28, 19, sidewall_right_bottom);
	draw_3d(29, 20, sidewall_right_bottom);
	draw_3d(28, 20, bottom);
	draw_3d(28, 21, bottom);
	draw_3d(29, 21, bottom);
	draw_3d(30, 21, sidewall_right_bottom);
}
void draw_lefttile_0() {
	int cx;
	for (cx = 0; cx < 7; cx++) { 
		draw_3d(cx, 19, bottom_forward);
	}
	for (cx = 0; cx < 6; cx++) { 
		draw_3d(cx, 20, bottom);
	}
	for (cx = 0; cx < 5; cx++) { 
		draw_3d(cx, 21, bottom);
	}
	draw_3d(7, 19, bottom_left);
	draw_3d(6, 20, bottom_left);
	draw_3d(5, 21, bottom_left);
}
void draw_righttile_0() {
	int cx;
	for (cx = 29; cx < 36; cx++) { 
		draw_3d(cx, 19, bottom_forward);
	}
	for (cx = 30; cx < 36; cx++) { 
		draw_3d(cx, 20, bottom);
	}
	for (cx = 31; cx < 36; cx++) { 
		draw_3d(cx, 21, bottom);
	}
	draw_3d(28, 19, bottom_right);
	draw_3d(29, 20, bottom_right);
	draw_3d(30, 21, bottom_right);
}
void draw_leftwall_0() {
	int counter;
	for (counter = 0; counter < 19; counter++) {
		draw_3d(5, counter, sidewall_left);
		draw_3d(6, counter, sidewall_left);
		draw_3d(7, counter, sidewall_left_side);
	}
	draw_3d(5, 19, sidewall_left);
	draw_3d(6, 19, sidewall_left);
	draw_3d(5, 20, sidewall_left);
}
void draw_rightwall_0() {
	int counter;
	for (counter = 0; counter < 19; counter++) {
		draw_3d(28, counter, sidewall_right_side);
		draw_3d(29, counter, sidewall_right);
		draw_3d(30, counter, sidewall_right);
	}
	draw_3d(29, 19, sidewall_right);
	draw_3d(30, 19, sidewall_right);
	draw_3d(30, 20, sidewall_right);
}
void draw_frontwall_0() {
	int cx, cy;
	for (cx = 8; cx < 28; cx++) {
		for (cy = 0; cy < 19; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_leftfrontwall_0() {
	int cx, cy;
	for (cx = 0; cx < 8; cx++) {
		for (cy = 0; cy < 19; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_rightfrontwall_0() {
	int cx, cy;
	for (cx = 28; cx < 36; cx++) {
		for (cy = 0; cy < 19; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_tile_1() {
	draw_3d(10, 16, sidewall_left_bottom);
	draw_3d(9, 17, sidewall_left_bottom);
	draw_3d(10, 17, bottom);
	draw_3d(8, 18, sidewall_left_bottom);
	draw_3d(9, 18, bottom);
	draw_3d(10, 18, bottom);
	for (cx = 11; cx < 25; cx++) {
		draw_3d(cx, 16, bottom_forward);
	}
	for (cx = 11; cx < 25; cx++) {
		for (cy = 17; cy < 19; cy++) { 
			draw_3d(cx, cy, bottom);
		}
	}
	draw_3d(25, 16, sidewall_right_bottom);
	draw_3d(25, 17, bottom);
	draw_3d(26, 17, sidewall_right_bottom);
	draw_3d(25, 18, bottom);
	draw_3d(26, 18, bottom);
	draw_3d(27, 18, sidewall_right_bottom);
}
void draw_leftwall_1() {
	draw_3d(8, 0, sidewall_left);
	draw_3d(9, 0, sidewall_left_top);
	draw_3d(10, 0, blank);
	draw_3d(8, 1, sidewall_left);
	draw_3d(9, 1, sidewall_left);
	draw_3d(10, 1, sidewall_left_top);
	for (counter = 2; counter < 16; counter++) {
		draw_3d(8, counter, sidewall_left);
		draw_3d(9, counter, sidewall_left);
		draw_3d(10, counter, sidewall_left_side);
	}
	draw_3d(8, 16, sidewall_left);
	draw_3d(9, 16, sidewall_left);
	draw_3d(8, 17, sidewall_left);
}
void draw_rightwall_1() {
	int counter;
	draw_3d(25, 0, blank);
	draw_3d(26, 0, sidewall_right_top);
	draw_3d(27, 0, sidewall_right);
	draw_3d(25, 1, sidewall_right_top);
	draw_3d(26, 1, sidewall_right);
	draw_3d(27, 1, sidewall_right);
	for (counter = 2; counter < 16; counter++) {
		draw_3d(25, counter, sidewall_right_side);
		draw_3d(26, counter, sidewall_right);
		draw_3d(27, counter, sidewall_right);
	}
	draw_3d(26, 16, sidewall_right);
	draw_3d(27, 16, sidewall_right);
	draw_3d(27, 17, sidewall_right);
}
void draw_frontwall_1() {
	int cx, cy;
	for (cx = 11; cx < 25; cx++) {
		for (cy = 2; cy < 16; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_leftslim_1() {
	for (counter = 0; counter < 19; counter++) {
		draw_3d(7, counter, slimwall);
	}
	for (cx = 0; cx < 7; cx++) {
		draw_3d(cx,2,flattop);
		draw_3d(cx, 16, bottom_forward);
		for (cy = 17; cy < 19; cy++) { 
			draw_3d(cx, cy, bottom);
		}
	}
}
void draw_leftslim_1_proto(bool viewblocked) {
	for (counter = 0; counter < 19; counter++) {
		draw_3d(7, counter, slimwall);
	}
	for (cx = 0; cx < 7; cx++) {
		draw_3d(cx,2,flattop);
		if(!viewblocked) {
			for (cy = 2; cy < 16; cy++) { 
				draw_3d(cx, cy, frontwall);
			}			
		} else {
			draw_3d(cx, 13, bottom_forward);	
			for (cy = 14; cy < 16; cy++) { 
				draw_3d(cx, cy, bottom);
			}	
		}
		draw_3d(cx, 16, bottom_forward);
		for (cy = 17; cy < 19; cy++) { 
			draw_3d(cx, cy, bottom);
		}
	}
}
void draw_rightslim_1() {
	for (counter = 0; counter < 19; counter++) {
		draw_3d(28, counter, slimwall);
	}
	for (cx = 29; cx < 36; cx++) {
		for (cy = 16; cy < 19; cy++) { 
			draw_3d(cx, cy, bottom);
		}
		draw_3d(cx,2,flattop);
	}
}
void draw_lefttile_1() {
	draw_3d( 8, 16, bottom_forward);
	draw_3d( 9, 16, bottom_forward);
	draw_3d( 8, 17, bottom);
	draw_3d( 8, 18, bottom_left);
	draw_3d( 9, 17, bottom_left);
	draw_3d(10, 16, bottom_left);
}
void draw_righttile_1() {
	draw_3d(26, 16, bottom_forward);
	draw_3d(27, 16, bottom_forward);
	draw_3d(27, 17, bottom);
	draw_3d(25, 16, bottom_right);
	draw_3d(26, 17, bottom_right);
	draw_3d(27, 18, bottom_right);
}
void draw_leftfrontwall_1() {
	for (cx = 8; cx < 11; cx++) {
		for (cy = 2; cy < 16; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_rightfrontwall_1() {
	for (cx = 25; cx < 28; cx++) {
		for (cy = 2; cy < 16; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_tile_2() {
	draw_3d(13, 13, sidewall_left_bottom);
	draw_3d(12, 14, sidewall_left_bottom);
	draw_3d(13, 14, bottom);
	draw_3d(11, 15, sidewall_left_bottom);
	draw_3d(12, 15, bottom);
	draw_3d(13, 15, bottom);
	for (cx = 14; cx < 22; cx++) {
		draw_3d(cx, 13, bottom_forward);
	}
	for (cx = 14; cx < 22; cx++) {
		for (cy = 14; cy < 16; cy++) { 
			draw_3d(cx, cy, bottom);
		}
	}
	draw_3d(22, 13, sidewall_right_bottom);
	draw_3d(22, 14, bottom);
	draw_3d(23, 14, sidewall_right_bottom);
	draw_3d(22, 15, bottom);
	draw_3d(23, 15, bottom);
	draw_3d(24, 15, sidewall_right_bottom);
}
void draw_frontwall_2() {
	int cx, cy;
	for (cx = 14; cx < 22; cx++) {
		for (cy = 5; cy < 13; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_leftwall_2() {
	draw_3d(11, 2, sidewall_left_top);
	draw_3d(12, 2, blank);
	draw_3d(13, 2, blank);
	draw_3d(11, 3, sidewall_left);
	draw_3d(12, 3, sidewall_left_top);
	draw_3d(13, 3, sidewall_left);
	draw_3d(11, 4, sidewall_left);
	draw_3d(12, 4, sidewall_left);
	draw_3d(13, 4, sidewall_left_top);
	for (counter = 5; counter < 13; counter++) {
		draw_3d(11, counter, sidewall_left);
		draw_3d(12, counter, sidewall_left);
		draw_3d(13, counter, sidewall_left_side);
	}
	draw_3d(11, 13, sidewall_left);
	draw_3d(12, 13, sidewall_left);
	draw_3d(11, 14, sidewall_left);
}
void draw_rightwall_2() {
	draw_3d(24, 2, sidewall_right_top);
	draw_3d(23, 2, blank);
	draw_3d(22, 2, blank);
	draw_3d(24, 3, sidewall_right);
	draw_3d(23, 3, sidewall_right_top);
	draw_3d(22, 3, sidewall_right);
	draw_3d(24, 4, sidewall_right);
	draw_3d(23, 4, sidewall_right);
	draw_3d(22, 4, sidewall_right_top);
	for (counter = 5; counter < 13; counter++) {
		draw_3d(24, counter, sidewall_right);
		draw_3d(23, counter, sidewall_right);
		draw_3d(22, counter, sidewall_right_side);
	}
	draw_3d(24, 13, sidewall_right);
	draw_3d(23, 13, sidewall_right);
	draw_3d(24, 14, sidewall_right);
}
void draw_lefttile_2() {
	draw_3d(11, 13, bottom_forward);
	draw_3d(12, 13, bottom_forward);
	draw_3d(11, 14, bottom);
	draw_3d(11, 15, bottom_left);
	draw_3d(12, 14, bottom_left);
	draw_3d(13, 13, bottom_left);
}
void draw_righttile_2() {
	draw_3d(23, 13, bottom_forward);
	draw_3d(24, 13, bottom_forward);
	draw_3d(24, 14, bottom);
	draw_3d(22, 13, bottom_right);
	draw_3d(23, 14, bottom_right);
	draw_3d(24, 15, bottom_right);
}
void draw_leftslim_2() {
	draw_3d(10,  1, flattop);
	for (counter = 2; counter < 16; counter++) {
		draw_3d(10, counter, slimwall);
	}
	for (cy = 13; cy < 16; cy++) { 
		draw_3d(8, cy, bottom);
		draw_3d(9, cy, bottom);
	}
	draw_3d(8, 4, flattop);
	draw_3d(9, 4, flattop);

}
void draw_rightslim_2() {
	draw_3d(25,  1, flattop);
	for (counter = 2; counter < 16; counter++) {
		draw_3d(25, counter, slimwall);
	}
	for (cy = 13; cy < 16; cy++) { 
		draw_3d(26, cy, bottom);
		draw_3d(27, cy, bottom);
	}
	draw_3d(26, 4, flattop);
	draw_3d(27, 4, flattop);
}
void draw_leftfrontwall_2() {
	for (cx = 11; cx < 14; cx++) {
		for (cy = 5; cy < 13; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_rightfrontwall_2() {
	for (cx = 22; cx < 25; cx++) {
		for (cy = 5; cy < 13; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_tile_3() {
	draw_3d(15, 11, sidewall_left_bottom);
	draw_3d(14, 12, sidewall_left_bottom);
	draw_3d(15, 12, bottom);
	for (cx = 16; cx < 20; cx++) {
		draw_3d(cx, 11, bottom_forward);
	}
	for (cx = 16; cx < 20; cx++) {
		draw_3d(cx, 12, bottom);
	}
	draw_3d(20, 11, sidewall_right_bottom);
	draw_3d(20, 12, bottom);
	draw_3d(21, 12, sidewall_right_bottom);
}
void draw_frontwall_3() {
	int cx, cy;
	for (cx = 16; cx < 20; cx++) {
		for (cy = 7; cy < 11; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_leftwall_3() {
	draw_3d(14, 5, sidewall_left_top);
	draw_3d(15, 5, blank);
	draw_3d(14, 6, sidewall_left);
	draw_3d(15, 6, sidewall_left_top);
	for (counter = 7; counter < 11; counter++) {
		draw_3d(14, counter, sidewall_left);
		draw_3d(15, counter, sidewall_left_side);
	}
	draw_3d(14, 11, sidewall_left);
}
void draw_rightwall_3() {
	draw_3d(21, 5, sidewall_right_top);
	draw_3d(20, 5, blank);
	draw_3d(21, 6, sidewall_right);
	draw_3d(20, 6, sidewall_right_top);
	for (counter = 7; counter < 11; counter++) {
		draw_3d(21, counter, sidewall_right);
		draw_3d(20, counter, sidewall_right_side);
	}
	draw_3d(21, 11, sidewall_right);
}
void draw_lefttile_3() {
	draw_3d(14, 11, bottom_forward);
	draw_3d(14, 12, bottom_left);
	draw_3d(15, 11, bottom_left);
}
void draw_righttile_3() {
	draw_3d(21, 11, bottom_forward);
	draw_3d(20, 11, bottom_right);
	draw_3d(21, 12, bottom_right);
}
void draw_leftslim_3() {
	draw_3d(13,  4, flattop);
	for (counter = 5; counter < 13; counter++) {
		draw_3d(13, counter, slimwall);
	}
	draw_3d(11, 11, bottom);
	draw_3d(11, 12, bottom);
	draw_3d(12, 11, bottom);
	draw_3d(12, 12, bottom);
	draw_3d(11,  6, flattop);
	draw_3d(12,  6, flattop);

}
void draw_rightslim_3() {
	draw_3d(22,  4, flattop);
	for (counter = 5; counter < 13; counter++) {
		draw_3d(22, counter, slimwall);
	}
	draw_3d(23, 11, bottom);
	draw_3d(23, 12, bottom);
	draw_3d(24, 11, bottom);
	draw_3d(24, 12, bottom);
	draw_3d(23,  6, flattop);
	draw_3d(24,  6, flattop);

}
void draw_leftfrontwall_3() {
	for (cx = 14; cx < 16; cx++) {
		for (cy = 7; cy < 11; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_rightfrontwall_3() {
	for (cx = 20; cx < 22; cx++) {
		for (cy = 7; cy < 11; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_tile_4() {
	draw_3d(16, 10, sidewall_left_bottom);
	draw_3d(17, 10, bottom_forward);
	draw_3d(18, 10, bottom_forward);
	draw_3d(19, 10, sidewall_right_bottom);
}
void draw_frontwall_4() {
	int cx, cy;
	for (cx = 17; cx < 19; cx++) {
		for (cy = 8; cy < 10; cy++) { 
			draw_3d(cx, cy, frontwall);
		}
	}
}
void draw_leftwall_4() {
	draw_3d(16, 7, sidewall_left_top);
	draw_3d(16, 8, sidewall_left);
	draw_3d(16, 9, sidewall_left);
}
void draw_rightwall_4() {
	draw_3d(19, 7, sidewall_right_top);
	draw_3d(19, 8, sidewall_right);
	draw_3d(19, 9, sidewall_right);
}
void draw_lefttile_4() {
	draw_3d(16,10,bottom_left);
}
void draw_righttile_4() {
	draw_3d(19,10,bottom_right);
}
void draw_leftslim_4() {
	draw_3d(15,  6, flattop);
	for (counter = 7; counter < 11; counter++) {
		draw_3d(15, counter, slimwall);
	}
	draw_3d(14, 10, bottom);
	draw_3d(14,  7, flattop);
}
void draw_rightslim_4() {
	draw_3d(20,  6, flattop);
	for (counter = 7; counter < 11; counter++) {
		draw_3d(20, counter, slimwall);
	}
	draw_3d(21, 10, bottom);
	draw_3d(21,  7, flattop);
}
void draw_leftfrontwall_4() {
	draw_3d(16,8, frontwall);
	draw_3d(16,9, frontwall);
}
void draw_rightfrontwall_4() {
	draw_3d(19,8, frontwall);
	draw_3d(19,9, frontwall);
}

void draw_frontwall_5() {
	draw_3d( 17, 8, frontwall);	
	draw_3d( 17, 9, frontwall);	
	draw_3d( 18, 8, frontwall);	
	draw_3d( 18, 9, frontwall);	
}
void draw_nofrontwall_5() {
	draw_3d( 17, 7, flattop);	
	draw_3d( 18, 7, flattop);	
	draw_3d( 17, 8, sidewall_right_side);	
	draw_3d( 17, 9, sidewall_right_side);	
	draw_3d( 18, 8, sidewall_left_side);	
	draw_3d( 18, 9, sidewall_left_side);

}


void display_current_view_3d() {
	if (show_leftwall_0) { draw_leftwall_0();
	} else {
		if (show_leftfrontwall_0) { draw_leftfrontwall_0(); }  
		draw_lefttile_0();
	}
	if (show_rightwall_0) { draw_rightwall_0();
	} else {
		if (show_rightfrontwall_0) { draw_rightfrontwall_0(); } 
		draw_righttile_0();
	}
	if (show_frontwall_0) { draw_frontwall_0();
	} else {
		draw_tile_1();
		if (show_leftslim_1) { draw_leftslim_1(); } // Todo: Is that neccessary?
		if (show_rightslim_1) { draw_rightslim_1(); }
		if (show_leftwall_1) { draw_leftwall_1();
		} else {
			if (show_leftfrontwall_1) { draw_leftfrontwall_1(); 
			} else {
				if (show_leftslim_1) { draw_leftslim_1(); }
			}
			draw_lefttile_1();
		}
		if (show_rightwall_1) { draw_rightwall_1();
		} else {
			if (show_rightfrontwall_1) { draw_rightfrontwall_1();
			} else {
				if (show_rightslim_1) { draw_rightslim_1(); }
			}
			draw_righttile_1();
		}
		if (show_frontwall_1) { draw_frontwall_1();
		} else {
			draw_tile_2();
			if (show_leftslim_2) { draw_leftslim_2(); }
			if (show_rightslim_2) { draw_rightslim_2(); }
			if (show_leftwall_2) { draw_leftwall_2();
			} else {
				if (show_leftfrontwall_2) { draw_leftfrontwall_2(); 
				} else {
					if (show_leftslim_2) { draw_leftslim_2(); }
				}
				draw_lefttile_2();
			}
			if (show_rightwall_2) { draw_rightwall_2();
			} else {
				if (show_rightfrontwall_2) { draw_rightfrontwall_2();
				} else {
					if (show_rightslim_2) { draw_rightslim_2(); }
				}
				draw_righttile_2();
			}
			if (show_frontwall_2) { draw_frontwall_2();
			} else {
				draw_tile_3();
				if (show_leftslim_3) { draw_leftslim_3(); }
				if (show_rightslim_3) { draw_rightslim_3(); }
				if (show_leftwall_3) { draw_leftwall_3();
				} else {
					if (show_leftfrontwall_3) { draw_leftfrontwall_3(); 
					} else {
						if (show_leftslim_3) { draw_leftslim_3(); }
					}
					draw_lefttile_3();
				}
				if (show_rightwall_3) { draw_rightwall_3();
				} else {
					if (show_rightfrontwall_3) { draw_rightfrontwall_3();
					} else {
						if (show_rightslim_3) { draw_rightslim_3(); }
					}
					draw_righttile_3();
				}
				if (show_frontwall_3) { draw_frontwall_3();
				} else {
					draw_tile_4();
					if (show_leftslim_4) { draw_leftslim_4(); }
					if (show_rightslim_4) { draw_rightslim_4(); }
					if (show_leftwall_4) { draw_leftwall_4();
					} else {
						if (show_leftfrontwall_4) { draw_leftfrontwall_4(); 
						} else {
							if (show_leftslim_4) { draw_leftslim_4(); }
						}
						draw_lefttile_4();
					}
					if (show_rightwall_4) { draw_rightwall_4();
					} else {
						if (show_rightfrontwall_4) { draw_rightfrontwall_4();
						} else {
							if (show_rightslim_4) { draw_rightslim_4(); }
						}
						draw_righttile_4();
					}
					if (show_frontwall_4) { draw_frontwall_4();
					} else {
						if (show_frontwall_5) { draw_frontwall_5();
						} else {
							draw_nofrontwall_5();
						}
					}
				}
			}
		}
	}
}


void display_maze_3d(int playerx, int playery, int orientation) {
	blank_3d(); // TODO: Only for debugging
	draw_tile_0();

	switch (orientation) {
		// FIXME: When adding and subtracting from the player's position, we will go out of the array.
		// We then get an undefined value for the walls. 
		// Does this matter? There is always a closer wall to block the view.
	case UP:
		// Look up
			show_frontwall_0		= MAZE[playerx][playery].up;
			show_frontwall_1		= MAZE[playerx][playery-1].up;
			show_frontwall_2		= MAZE[playerx][playery-2].up;
			show_frontwall_3		= MAZE[playerx][playery-3].up;
			show_frontwall_4		= MAZE[playerx][playery-4].up;
			show_frontwall_5		= MAZE[playerx][playery-5].up;
			show_leftfrontwall_0 	= MAZE[playerx-1][playery].up;
			show_leftfrontwall_1 	= MAZE[playerx-1][playery-1].up;
			show_leftfrontwall_2 	= MAZE[playerx-1][playery-2].up;
			show_leftfrontwall_3 	= MAZE[playerx-1][playery-3].up;
			show_leftfrontwall_4 	= MAZE[playerx-1][playery-4].up;
			show_leftwall_0 		= MAZE[playerx][playery].left;
			show_leftwall_1 		= MAZE[playerx][playery-1].left;
			show_leftwall_2 		= MAZE[playerx][playery-2].left;
			show_leftwall_3 		= MAZE[playerx][playery-3].left;
			show_leftwall_4 		= MAZE[playerx][playery-4].left;
			show_rightfrontwall_0	= MAZE[playerx+1][playery].up;
			show_rightfrontwall_1	= MAZE[playerx+1][playery-1].up;
			show_rightfrontwall_2	= MAZE[playerx+1][playery-2].up;
			show_rightfrontwall_3	= MAZE[playerx+1][playery-3].up;
			show_rightfrontwall_4	= MAZE[playerx+1][playery-4].up;
			show_rightwall_0		= MAZE[playerx+1][playery].left;
			show_rightwall_1		= MAZE[playerx+1][playery-1].left;
			show_rightwall_2		= MAZE[playerx+1][playery-2].left;
			show_rightwall_3		= MAZE[playerx+1][playery-3].left;
			show_rightwall_4		= MAZE[playerx+1][playery-4].left;
			show_leftslim_1			= show_leftwall_1 
										&& !show_leftwall_0 
										&& !show_leftfrontwall_0;
			show_rightslim_1		= show_rightwall_1
										&& !show_rightwall_0
										&& !show_rightfrontwall_0;
			show_leftslim_2			= show_leftwall_2 
										&& !show_leftwall_1 
										&& !show_leftfrontwall_1;
			show_rightslim_2		= show_rightwall_2
										&& !show_rightwall_1
										&& !show_rightfrontwall_1;
			show_leftslim_3			= show_leftwall_3 
										&& !show_leftwall_2 
										&& !show_leftfrontwall_2;
			show_rightslim_3		= show_rightwall_3
										&& !show_rightwall_2
										&& !show_rightfrontwall_2;
			show_leftslim_4			= show_leftwall_4 
										&& !show_leftwall_3 
										&& !show_leftfrontwall_3;
			show_rightslim_4		= show_rightwall_4
										&& !show_rightwall_3
										&& !show_rightfrontwall_3;
		break;
	case LEFT: 
		// Look left
			show_frontwall_0		= MAZE[playerx][playery].left;
			show_frontwall_1		= MAZE[playerx-1][playery].left;
			show_frontwall_2		= MAZE[playerx-2][playery].left;
			show_frontwall_3		= MAZE[playerx-3][playery].left;
			show_frontwall_4		= MAZE[playerx-4][playery].left;
			show_frontwall_5		= MAZE[playerx-5][playery].left;
			show_leftfrontwall_0 	= MAZE[playerx][playery+1].left;
			show_leftfrontwall_1 	= MAZE[playerx-1][playery+1].left;
			show_leftfrontwall_2 	= MAZE[playerx-2][playery+1].left;
			show_leftfrontwall_3 	= MAZE[playerx-3][playery+1].left;
			show_leftfrontwall_4 	= MAZE[playerx-4][playery+1].left;
			show_leftwall_0 		= MAZE[playerx][playery+1].up;
			show_leftwall_1 		= MAZE[playerx-1][playery+1].up;
			show_leftwall_2 		= MAZE[playerx-2][playery+1].up;;
			show_leftwall_3 		= MAZE[playerx-3][playery+1].up;
			show_leftwall_4 		= MAZE[playerx-4][playery+1].up;
			show_rightfrontwall_0	= MAZE[playerx][playery-1].left;
			show_rightfrontwall_1	= MAZE[playerx-1][playery-1].left;
			show_rightfrontwall_2	= MAZE[playerx-2][playery-1].left;
			show_rightfrontwall_3	= MAZE[playerx-3][playery-1].left;
			show_rightfrontwall_4	= MAZE[playerx-4][playery-1].left;
			show_rightwall_0		= MAZE[playerx][playery].up;
			show_rightwall_1		= MAZE[playerx-1][playery].up;
			show_rightwall_2		= MAZE[playerx-2][playery].up;
			show_rightwall_3		= MAZE[playerx-3][playery].up;
			show_rightwall_4		= MAZE[playerx-4][playery].up;
			show_leftslim_1			= show_leftwall_1 
										&& !show_leftwall_0 
										&& !show_leftfrontwall_0;
			show_rightslim_1		= show_rightwall_1
										&& !show_rightwall_0
										&& !show_rightfrontwall_0;
			show_leftslim_2			= show_leftwall_2 
										&& !show_leftwall_1 
										&& !show_leftfrontwall_1;
			show_rightslim_2		= show_rightwall_2
										&& !show_rightwall_1
										&& !show_rightfrontwall_1;
			show_leftslim_3			= show_leftwall_3 
										&& !show_leftwall_2 
										&& !show_leftfrontwall_2;
			show_rightslim_3		= show_rightwall_3
										&& !show_rightwall_2
										&& !show_rightfrontwall_2;
			show_leftslim_4			= show_leftwall_4 
										&& !show_leftwall_3 
										&& !show_leftfrontwall_3;
			show_rightslim_4		= show_rightwall_4
										&& !show_rightwall_3
										&& !show_rightfrontwall_3;
		break;
	case RIGHT:
		// Look right
			show_frontwall_0		= MAZE[playerx+1][playery].left;
			show_frontwall_1		= MAZE[playerx+2][playery].left;
			show_frontwall_2		= MAZE[playerx+3][playery].left;
			show_frontwall_3		= MAZE[playerx+4][playery].left;
			show_frontwall_4		= MAZE[playerx+5][playery].left;
			show_frontwall_5		= MAZE[playerx+6][playery].left;
			show_leftfrontwall_0 	= MAZE[playerx+1][playery-1].left;
			show_leftfrontwall_1 	= MAZE[playerx+2][playery-1].left;
			show_leftfrontwall_2 	= MAZE[playerx+3][playery-1].left;
			show_leftfrontwall_3 	= MAZE[playerx+4][playery-1].left;
			show_leftfrontwall_4 	= MAZE[playerx+5][playery-1].left;
			show_leftwall_0 		= MAZE[playerx][playery].up;
			show_leftwall_1 		= MAZE[playerx+1][playery].up;
			show_leftwall_2 		= MAZE[playerx+2][playery].up;
			show_leftwall_3 		= MAZE[playerx+3][playery].up;
			show_leftwall_4 		= MAZE[playerx+4][playery].up;
			show_rightfrontwall_0	= MAZE[playerx+1][playery+1].left;
			show_rightfrontwall_1	= MAZE[playerx+2][playery+1].left;
			show_rightfrontwall_2	= MAZE[playerx+3][playery+1].left;
			show_rightfrontwall_3	= MAZE[playerx+4][playery+1].left;
			show_rightfrontwall_4	= MAZE[playerx+5][playery+1].left;;
			show_rightwall_0		= MAZE[playerx][playery+1].up;
			show_rightwall_1		= MAZE[playerx+1][playery+1].up;
			show_rightwall_2		= MAZE[playerx+2][playery+1].up;
			show_rightwall_3		= MAZE[playerx+3][playery+1].up;
			show_rightwall_4		= MAZE[playerx+4][playery+1].up;
			show_leftslim_1			= show_leftwall_1 
										&& !show_leftwall_0 
										&& !show_leftfrontwall_0;
			show_rightslim_1		= show_rightwall_1
										&& !show_rightwall_0
										&& !show_rightfrontwall_0;
			show_leftslim_2			= show_leftwall_2 
										&& !show_leftwall_1 
										&& !show_leftfrontwall_1;
			show_rightslim_2		= show_rightwall_2
										&& !show_rightwall_1
										&& !show_rightfrontwall_1;
			show_leftslim_3			= show_leftwall_3 
										&& !show_leftwall_2 
										&& !show_leftfrontwall_2;
			show_rightslim_3		= show_rightwall_3
										&& !show_rightwall_2
										&& !show_rightfrontwall_2;
			show_leftslim_4			= show_leftwall_4 
										&& !show_leftwall_3 
										&& !show_leftfrontwall_3;
			show_rightslim_4		= show_rightwall_4
										&& !show_rightwall_3
										&& !show_rightfrontwall_3;
		break;
	case DOWN:
		// Look down
			show_frontwall_0		= MAZE[playerx][playery+1].up;
			show_frontwall_1		= MAZE[playerx][playery+2].up;
			show_frontwall_2		= MAZE[playerx][playery+3].up;
			show_frontwall_3		= MAZE[playerx][playery+4].up;
			show_frontwall_4		= MAZE[playerx][playery+5].up;
			show_frontwall_5		= MAZE[playerx][playery+6].up;
			show_leftfrontwall_0 	= MAZE[playerx+1][playery+1].up;
			show_leftfrontwall_1 	= MAZE[playerx+1][playery+2].up;
			show_leftfrontwall_2 	= MAZE[playerx+1][playery+3].up;
			show_leftfrontwall_3 	= MAZE[playerx+1][playery+4].up;
			show_leftfrontwall_4 	= MAZE[playerx+1][playery+5].up;
			show_leftwall_0 		= MAZE[playerx+1][playery].left;
			show_leftwall_1 		= MAZE[playerx+1][playery+1].left;
			show_leftwall_2 		= MAZE[playerx+1][playery+2].left;
			show_leftwall_3 		= MAZE[playerx+1][playery+3].left;
			show_leftwall_4 		= MAZE[playerx+1][playery+4].left;
			show_rightfrontwall_0	= MAZE[playerx-1][playery+1].up;
			show_rightfrontwall_1	= MAZE[playerx-1][playery+2].up;
			show_rightfrontwall_2	= MAZE[playerx-1][playery+3].up;
			show_rightfrontwall_3	= MAZE[playerx-1][playery+4].up;
			show_rightfrontwall_4	= MAZE[playerx-1][playery+5].up;
			show_rightwall_0		= MAZE[playerx][playery].left;
			show_rightwall_1		= MAZE[playerx][playery+1].left;
			show_rightwall_2		= MAZE[playerx][playery+2].left;
			show_rightwall_3		= MAZE[playerx][playery+3].left;
			show_rightwall_4		= MAZE[playerx][playery+4].left;
			show_leftslim_1			= show_leftwall_1 
										&& !show_leftwall_0 
										&& !show_leftfrontwall_0;
			show_rightslim_1		= show_rightwall_1
										&& !show_rightwall_0
										&& !show_rightfrontwall_0;
			show_leftslim_2			= show_leftwall_2 
										&& !show_leftwall_1 
										&& !show_leftfrontwall_1;
			show_rightslim_2		= show_rightwall_2
										&& !show_rightwall_1
										&& !show_rightfrontwall_1;
			show_leftslim_3			= show_leftwall_3 
										&& !show_leftwall_2 
										&& !show_leftfrontwall_2;
			show_rightslim_3		= show_rightwall_3
										&& !show_rightwall_2
										&& !show_rightfrontwall_2;
			show_leftslim_4			= show_leftwall_4 
										&& !show_leftwall_3 
										&& !show_leftfrontwall_3;
			show_rightslim_4		= show_rightwall_4
										&& !show_rightwall_3
										&& !show_rightfrontwall_3;
		break;	
	} // end switch (orientation)
	display_current_view_3d();
}