// MAZE x16
//
// Based on:
//Purpy Pupple's amazing maze generator. 
//Released under the CC-BY-SA 3.0 License and the GFDL
// https://en.wikipedia.org/wiki/User:Dllu/Maze

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <cbm.h>

#define UP 0     //-y
#define DOWN 1   //+y
#define LEFT 2   //-x
#define RIGHT 3  //+x

#define XOFFSET 20
#define YOFFSET 20

#define WHITE printf("%c", ' ')
#define BLACK printf("%c", 'M')
#define GREY printf("%c", 'm')
#define RED   printf("%c", '.')

long numin=1;     //Number of cells (already) in the maze.

// const int xsize=19;     // labyrinth.bas maximum
// const int ysize=10;     // labyrinth.bas maximum
#define xsize 19
#define ysize 10

struct cell{
	bool in;  //Is this cell in the maze?
	bool up;  //Does the wall above this cell exist?
	bool left;//Does the wall to the left of this cell exist?
	int prevx, prevy; //The coordinates of the previous cell, used for backtracking.
};

struct cell MAZE[xsize][ysize];

enum block {
    wall = 0xa0,
    path = 0x20,
    up = 0x1e,
    down = 0x16,
    left = 0x28,
    right = 0x29,
};

void initialize();
void generate();
void display(int xspecial, int yspecial); // was: savebmp()
void draw(int x, int y, enum block element);

int main(){
    // https://www.commanderx16.com/forum/index.php?/topic/999-cc65-switch-to-uppercasepetscii/
    cbm_k_bsout(CH_FONT_UPPER);
    //seed random number generator with system time
	srand((unsigned int)time(NULL)); 
	initialize();      //initialize the maze
	generate();        //generate the maze
    display(3, 3); // display the maze

    return 0;
}

void initialize(){
	//Initialize the maze!
    int x, y;
	for(x=0;x<xsize;x++){
		for(y=0;y<ysize;y++){
			//The maze cells on the edges of the maze are "in" to provide padding. Otherwise, all maze cells are not in.
			MAZE[x][y].in   = (x==0||x==xsize-1||y==0||y==ysize-1)?1:0;
			//All maze cells have all walls existing by default, except the perimeter cells.
			MAZE[x][y].up   = (x==0||x==xsize-1||y==0)?0:1;
			MAZE[x][y].left = (x==0||y==0||y==ysize-1)?0:1;
		}
	}
	return;
}

void generate() {	
    //start growing from the corner. It could theoretically start growing from anywhere, doesn't matter.
    int xcur=1, ycur=1;
	int whichway;
	bool success;
	MAZE[xcur][ycur].in = true;
	do{
		while( MAZE[xcur][ycur-1].in&&MAZE[xcur][ycur+1].in&&
			   MAZE[xcur-1][ycur].in&&MAZE[xcur+1][ycur].in ){
				   //If all the neighbourhood cells are in, backtrack.
				int xcur2=MAZE[xcur][ycur].prevx;
				ycur=MAZE[xcur][ycur].prevy;
				xcur=xcur2;
		}
		do{
			//Randomly grow the maze if possible.
			success=0;
			whichway=rand()%4;
			switch(whichway){
			case UP:
				if(!MAZE[xcur][ycur-1].in){
					success=1;
					MAZE[xcur][ycur].up=0;
					MAZE[xcur][ycur-1].prevx=xcur;
					MAZE[xcur][ycur-1].prevy=ycur;
					ycur--;
				}
				break;
			case DOWN:
				if(!MAZE[xcur][ycur+1].in){
					success=1;
					MAZE[xcur][ycur+1].up=0;
					MAZE[xcur][ycur+1].prevx=xcur;
					MAZE[xcur][ycur+1].prevy=ycur;
					ycur++;
				}
				break;
			case LEFT:
				if(!MAZE[xcur-1][ycur].in){
					success=1;
					MAZE[xcur][ycur].left=0;
					MAZE[xcur-1][ycur].prevx=xcur;
					MAZE[xcur-1][ycur].prevy=ycur;
					xcur--;
				}
				break;
			case RIGHT:
				if(!MAZE[xcur+1][ycur].in){
					success=1;
					MAZE[xcur+1][ycur].left=0;
					MAZE[xcur+1][ycur].prevx=xcur;
					MAZE[xcur+1][ycur].prevy=ycur;
					xcur++;
				}
				break;
			}
		}while(!success);
		MAZE[xcur][ycur].in=1;
		numin++; //Every iteration of this loop, one maze cell is added to the maze.
	}while(numin<(xsize-2)*(ysize-2));
	return;
}

void draw(int x, int y, enum block element){
	int position, color = 0;

	position = ((y + YOFFSET) * 0x0100 ) + (x + XOFFSET) * 2;
	color = position + 1; 
	vpoke((int)element,position);
	vpoke(0x61,color);
}

void display(int xspecial, int yspecial) {
    // xspecial, yspecial ---> You are here
    int x, y;
	int width=(xsize-1)*2-1;
	int height=(ysize-1)*2-1;

	for(y = 0; y <= height - 1; y++){
		for(x = 0; x <= width - 1; x++){
			if(x%2 == 1 && y%2 == 1) {
            // We are on the path or on a wall
				if(x/2+1 == xspecial && y/2+1 == yspecial) {
                    // ---> You are here
                    RED;
                } else {
                    // Shrink back to original size
                    // Fill according to "in" 
                    // or "out" (not possible in finished maze)
					if(MAZE[x/2+1][y/2+1].in) {
                        WHITE;
                    } else {
                       GREY;
                    }
				}
            // We are "on a boundary"
			} else if (x%2 == 0 && y%2 == 0) {  
				BLACK;
			} else if (x%2 == 0 && y%2 == 1) {
				if(MAZE[x/2+1][y/2+1].left) {
                    BLACK;
                } else {
                    WHITE;
                }
			} else if (x%2 == 1 && y%2 == 0) {
				if(MAZE[x/2+1][y/2+1].up) {
                    BLACK;
                } else {
                    WHITE;
                }
			}
		}
        printf("\n");
    }
}
